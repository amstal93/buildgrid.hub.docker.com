#!/bin/sh

export BUCKET_NAME=$(cat /config/s3-bucket-name)
export MINIO_ACCESS_KEY=$(cat /config/s3-access-key)
export MINIO_SECRET_KEY=$(cat /config/s3-secret-key)

# Create bucket
mkdir -p /mnt/data/${BUCKET_NAME}

# Start minio
minio server /mnt/data

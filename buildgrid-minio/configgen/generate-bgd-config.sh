#!/bin/sh

echo -n $BUCKET_NAME > /config/s3-bucket-name
echo "Bucket Name: $BUCKET_NAME"

echo "Bucket Endpoint: $BUCKET_ENDPOINT"
echo -n $BUCKET_ENDPOINT > /config/s3-endpoint

if [ -z "$BUCKET_ACCESS_KEY" ]; then
    echo "Generating Access Key"
    export BUCKET_ACCESS_KEY=`pwgen -s 30 1`
fi
echo -n $BUCKET_ACCESS_KEY > /config/s3-access-key
echo "Bucket Access Key: $BUCKET_ACCESS_KEY"

if [ -z $BUCKET_SECRET_KEY ]; then
    echo "Generating Secret Key"
    export BUCKET_SECRET_KEY=`pwgen -s 30 1`
fi
echo "Bucket Secret Key: $BUCKET_SECRET_KEY"
echo -n $BUCKET_SECRET_KEY > /config/s3-secret-key

# Copy those in /config/
# Allows us to have a mounted volume that can be shared
# between BuildGrid and minio containers
cp start-minio.sh /config/
cp bgd-s3.conf /config/
